<?php
/**
 * Data-Types for Rodneyrehm_Plist as defined by Apple.
 * {@link http://developer.apple.com/documentation/Darwin/Reference/ManPages/man5/plist.5.html Property Lists}
 * @author Rodney Rehm <rodney.rehm@medialize.de>
 * @author Christian Kruse <cjk@wwwtech.de>
 * @author Anton Swartz <aswartz@talefi.net>
 * @package rodneyrehm
 * @version $Id$
 */

/**
 * Base-Class of all Rodneyrehm_Plist_Types used by Rodneyrehm_Plist
 * @author Rodney Rehm <rodney.rehm@medialize.de>
 * @author Christian Kruse <cjk@wwwtech.de>
 * @author Anton Swartz <aswartz@talefi.net>
 * @package rodneyrehm
 * @subpackage rodneyrehm.types
 * @version $Id$
 */
abstract class Rodneyrehm_Plist_Type {
    /**
     * Rodneyrehm_Plist_Type nodes
     * @var array
     */
    protected $value = null;

    /**
     * Create new Rodneyrehm_Plist_Type.
     * @param mixed $value Value of Rodneyrehm_Plist_Type
     */
    public function __construct($value=null) {
        $this->setValue($value);
    }

    /************************************************************************************************
     *    M A G I C   P R O P E R T I E S
     ************************************************************************************************/

    /**
     * Get the Rodneyrehm_Plist_Type's value
     * @return mixed Rodneyrehm_Plist_Type's value
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Set the Rodneyrehm_Plist_Type's value
     * @return void
     */
    public function setValue($value) {
        $this->value = $value;
    }

    /************************************************************************************************
     *    S E R I A L I Z I N G
     ************************************************************************************************/
 
    /**
     * Get XML-Node.
     * @param DOMDocument $doc DOMDocument to create DOMNode in
     * @param string $nodeName Name of element to create
     * @return DOMNode Node created based on Rodneyrehm_Plist_Type
     * @uses $value as nodeValue
     */
    public function toXML(DOMDocument $doc, $nodeName) {
        $text = $doc->createTextNode($this->value);
        $node = $doc->createElement($nodeName);
        $node->appendChild($text);
        return $node;
    }

    /**
     * convert value to binary representation
     * @param Rodneyrehm_Binary The binary property list object
     * @return The offset in the object table
     */
    public abstract function toBinary(Rodneyrehm_Binary &$bplist);

    /**
     * Get Rodneyrehm_Plist_Type's value.
     * @return mixed primitive value
     * @uses $value for retrieving primitive of Rodneyrehm_Plist_Type
     */
    public function toArray() {
        return $this->getValue();
    }

}