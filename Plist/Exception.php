<?php
/**
 * Rodneyrehm_Plist
 * {@link http://developer.apple.com/documentation/Darwin/Reference/ManPages/man5/plist.5.html Property Lists}
 * @author Rodney Rehm <rodney.rehm@medialize.de>
 * @author Christian Kruse <cjk@wwwtech.de>
 * @author Anton Swartz <aswartz@talefi.net>
 * @package rodneyrehm
 * @version $Id$
 */

/**
 * Exception for errors with the PList format
 * @author Rodney Rehm <rodney.rehm@medialize.de>
 * @author Christian Kruse <cjk@wwwtech.de>
 * @author Anton Swartz <aswartz@talefi.net>
 * @package rodneyrehm
 */
class Rodneyrehm_Plist_Exception extends Exception {
}