<?php
/**
 * Rodneyrehm_Plist
 * {@link http://developer.apple.com/documentation/Darwin/Reference/ManPages/man5/plist.5.html Property Lists}
 * @author Rodney Rehm <rodney.rehm@medialize.de>
 * @author Christian Kruse <cjk@wwwtech.de>
 * @author Anton Swartz <aswartz@talefi.net>
 * @package rodneyrehm
 * @version $Id$
 */
 
/**
 * Boolean Type of Rodneyrehm_Plist
 * @author Rodney Rehm <rodney.rehm@medialize.de>
 * @author Christian Kruse <cjk@wwwtech.de>
 * @author Anton Swartz <aswartz@talefi.net>
 * @package rodneyrehm
 * @subpackage rodneyrehm.types
 */
class Rodneyrehm_Plist_Type_Boolean extends Rodneyrehm_Plist_Type {
    /**
     * Get XML-Node.
     * Returns &lt;true&gt; if $value is a true, &lt;false&gt; if $value is false.
     * @param DOMDocument $doc DOMDocument to create DOMNode in
     * @param string $nodeName For compatibility reasons; just ignore it
     * @return DOMNode &lt;true&gt; or &lt;false&gt;-Element
     */
    public function toXML(DOMDocument $doc,$nodeName="") {
        return $doc->createElement($this->value ? 'true' : 'false');
    }

    /**
     * convert value to binary representation
     * @param Rodneyrehm_Binary The binary property list object
     * @return The offset in the object table
     */
    public function toBinary(Rodneyrehm_Binary &$bplist) {
        return $bplist->boolToBinary($this->value);
    }

}