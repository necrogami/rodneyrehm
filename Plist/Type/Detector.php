<?php

/**
 * Rodneyrehm_Plist_Type_Detector
 * Interface for converting native PHP data structures to Rodneyrehm_Plist objects.
 * @author Rodney Rehm <rodney.rehm@medialize.de>
 * @author Christian Kruse <cjk@wwwtech.de>
 * @author Anton Swartz <aswartz@talefi.net>
 * @package rodneyrehm
 * @subpackage rodneyrehm.types
*/

class Rodneyrehm_Plist_Type_Detector {
  
  /**
   * flag stating if all arrays should automatically be converted to {@link Rodneyrehm_Plist_Type_Dictionary}
   * @var boolean
   */
  protected $autoDictionary = false;
  
  /**
   * flag stating if exceptions should be suppressed or thrown
   * @var boolean
   */
  protected $suppressExceptions = false;

  /**
   * name of a method that will be used for array to object conversations
   * @var boolean
   */
  protected $objectToArrayMethod = false;


  /**
   * Create new Rodneyrehm_Plist_Type_Detector
   * @param boolean $autoDicitionary if set to true all arrays will be converted to {@link Rodneyrehm_Plist_Type_Dictionary}
   * @param boolean $suppressExceptions if set to true toType() will not throw any exceptions
   * @param boolean $objectToArrayMethod if non-null, this method will be called on objects (if possible) to convert the object to an array
   */
  public function __construct($autoDicitionary=false,$suppressExceptions=false,$objectToArrayMethod=null) {
    $this->autoDicitionary = $autoDicitionary;
    $this->suppressExceptions = $suppressExceptions;
    $this->objectToArrayMethod = $objectToArrayMethod;
  }
  
  /**
   * Determine if an array is associative or numerical.
   * Numerical Arrays have incrementing index-numbers that don't contain gaps.
   * @param array $value Array to check indexes of
   * @return boolean true if array is associative, false if array has numeric indexes
   */
  protected function isAssociativeArray($value) {
    $numericKeys = true;
    $i = 0;
    foreach($value as $key => $v) {
      if($i !== $key) {
        $numericKeys = false;
        break;
      }
      $i++;
    }
    return !$numericKeys;
  }
  
  /**
   * Get the default value
   * @return Rodneyrehm_Plist_Type the default value to return if no suitable type could be determined
   */
  protected function defaultValue() {
    return new Rodneyrehm_Plist_Type_String();
  }
  
  /**
   * Create Rodneyrehm_Plist_Type-structure by guessing the data-types.
   * {@link Rodneyrehm_Plist_Type_Array}, {@link Rodneyrehm_Plist_Type_Dictionary}, {@link Rodneyrehm_Plist_Type_Boolean}, {@link Rodneyrehm_Plist_Type_Number} and {@link Rodneyrehm_Plist_Type_String} can be created, {@link Rodneyrehm_Plist_Type_Date} and {@link Rodneyrehm_Plist_Type_Data} cannot.
   * <br /><b>Note:</b>Distinguishing between {@link Rodneyrehm_Plist_Type_Array} and {@link Rodneyrehm_Plist_Type_Dictionary} is done by examining the keys. 
   * Keys must be strictly incrementing integers to evaluate to a {@link Rodneyrehm_Plist_Type_Array}. 
   * Since PHP does not offer a function to test for associative arrays, 
   * this test causes the input array to be walked twice and thus work rather slow on large collections. 
   * If you work with large arrays and can live with all arrays evaluating to {@link Rodneyrehm_Plist_Type_Dictionary}, 
   * feel free to set the appropriate flag.
   * <br /><b>Note:</b> If $value is an instance of Rodneyrehm_Plist_Type it is simply returned.
   * <br /><b>Note:</b> If $value is neither a Rodneyrehm_Plist_Type, array, numeric, boolean nor string, it is omitted.
   * @param mixed $value Value to convert to Rodneyrehm_Plist_Type
   * @param boolean $autoDictionary if true {@link Rodneyrehm_Plist_Type_Array}-detection is bypassed and arrays will be returned as {@link Rodneyrehm_Plist_Type_Dictionary}.
   * @return Rodneyrehm_Plist_Type Rodneyrehm_Plist_Type based on guessed type
   * @uses isAssociativeArray() to check if an array only has numeric indexes
   */
  public function toType($value) {
    switch(true) {
      case $value instanceof Rodneyrehm_Plist_Type:
        return $value;
      break;

      case is_object($value):
        // DateTime should be Rodneyrehm_Plist_Type_Date
        if(class_exists( 'DateTime' ) && $value instanceof DateTime){
          return new Rodneyrehm_Plist_Type_Date($value->getTimestamp());
        }
        
        // convert possible objects to arrays, arrays will be arrays
        if($this->objectToArrayMethod && is_callable(array($value, $this->objectToArrayMethod))){
          $value = call_user_func( array( $value, $this->objectToArrayMethod ) );
        }
        
        if(!is_array($value)){
          if($this->suppressExceptions)
            return $this->defaultValue();

          throw new PListException('Could not determine Rodneyrehm_Plist_Type for object of type '. get_class($value));
        }
      /* break; omitted */

      case $value instanceof Iterator:
      case is_array($value):
        // test if $value is simple or associative array
        if(!$this->autoDictionary) {
          if(!$this->isAssociativeArray($value)) {
            $t = new Rodneyrehm_Plist_Type_Array();
            foreach($value as $v) $t->add($this->toRodneyrehm_Plist_Type($v));
            return $t;
          }
        }

        $t = new Rodneyrehm_Plist_Type_Dictionary();
        foreach($value as $k => $v) $t->add($k, $this->toRodneyrehm_Plist_Type($v));

        return $t;
      break;

      case is_numeric($value):
        return new Rodneyrehm_Plist_Type_Number($value);
      break;

      case is_bool($value):
        return new Rodneyrehm_Plist_Type_Boolean($value);
      break;

      case is_string($value):
        return new Rodneyrehm_Plist_Type_String($value);
      break;

      case is_null($value):
        return new Rodneyrehm_Plist_Type_String();
      break;

      case is_resource($value):
        if( $this->suppressExceptions )
          return $this->defaultValue();

        throw new Rodneyrehm_Plist_Exception('Could not determine Rodneyrehm_Plist_Type for resource of type '. get_resource_type($value));
      break;
      
      default:
        if( $this->suppressExceptions )
          return $this->defaultValue();

        throw new Rodneyrehm_Plist_Exception('Could not determine Rodneyrehm_Plist_Type for '. gettype($value));
      break;
    }
  }

}

?>